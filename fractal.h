#include <fstream>
using namespace std;
#ifndef __fractal
#define __fractal
class fractal
{
protected:
int** image;
int height;
int width;
int colorConst;
public:
virtual void draw(int)=0;
virtual void color(int, int,int,int*)=0;
int* operator[](int i);
void cSquare(double& Imx, double& Rex);
int getXY(int x, int y);
friend ofstream& operator<<(ofstream& ofs, fractal& fr);
};
#endif