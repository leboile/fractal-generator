#include "mandelbrotJulia.h"
mandelbrotSet::mandelbrotSet(int w,int h,int c,int n)
{
 height=h;
 width=w;
 colorConst=c;
 image=new int*[w];
 for(int i=0; i<w; i++)
  image[i]=new int[h];
 for(int x=0; x<width; x++)
  for(int y=0; y<height; y++)
    image[x][y]=0;
 draw(n);
}
mandelbrotSet::~mandelbrotSet()
{
  for(int i=0; i<width; i++)
   delete[] image[i];
  delete[] image;
}
void mandelbrotSet::draw(int n)
{
  bool belong;
  int k=0;
  double Re,currzRe=0;
  double Im,currzIm=0;
  for(int y=0; y<height; y++)
   for(int x=0; x<width; x++)
   {
    Re=x*(3.5/width)-2; 
    Im=y*(2.0/height)-1;
	//cout<<Re<<" "<<Im<<endl;
	belong=true;
	currzRe=0;
	currzIm=0;
	k=0;
	while(k<n)
	{
	 k++;
	 cSquare(currzRe,currzIm);
	 currzRe+=Re;
	 currzIm+=Im;
	 if ((currzRe*currzRe+currzIm*currzIm>4) || (currzRe!=currzRe) || (currzIm!=currzIm))
	 {
	  belong=false;
	  break;
	 }
	}
	if (belong)
	 image[x][y]=1; else image[x][y]=k;
  }
}
juliaSet::juliaSet(int w,int h,int ax, int bx, int ay, int by, double cRe,double cIm,int c,int p, int n)
{
 height=h;
 width=w;
 power=p;
 minX=ax;
 maxX=bx;
 minY=ay;
 maxY=by;
 colorConst=c;
 juliaConstIm=cIm;
 juliaConstRe=cRe;
 image=new int*[w];
 for(int i=0; i<w; i++)
  image[i]=new int[h];
 for(int x=0; x<width; x++)
  for(int y=0; y<height; y++)
    image[x][y]=0;
 draw(n);
}
void juliaSet::draw(int n)
{
  int k=0;
  bool belong;
  double currzRe=0;
  double currzIm=0;
  for(int y=0; y<height; y++)
   for(int x=0; x<width; x++)
   {
    currzRe=(double)x*((double)(fabs(maxX)+fabs(minX))/(double)width)+minX; 
    currzIm=maxY-((double)y*((double)(fabs(maxY)+fabs(minY))/(double)height));
	belong=true;
	k=0;
	while(k<n)
	{
	 k++;
	 for(int i=0; i<power;i++)
	 cSquare(currzRe,currzIm);
	 currzRe+=juliaConstRe;
	 currzIm+=juliaConstIm;
	 if ((currzRe*currzRe+currzIm*currzIm>4)||(currzRe!=currzRe) || (currzIm!=currzIm))
	 {
	  belong=false;
	  break;
	 }
	}
	if (belong)
	 image[x][y]=1; else image[x][y]=k;
  }
}
void juliaSet::color(int i,int j, int k, int* rgb)
{
 rgb[0]=((image[i][j]*k)%255);
 rgb[1]=((image[i][j]*k)/255)*k%255;
 rgb[2]=(((image[i][j]*k)/255)/255)*k%255;
}
void mandelbrotSet::color(int i,int j, int k, int* rgb)
{
 rgb[0]=((image[i][j]*k)%255);
 rgb[1]=((image[i][j]*k)/255)*k%255;
 rgb[2]=(((image[i][j]*k)/255)/255)*k%255;
}
juliaSet::~juliaSet()
{
  for(int i=0; i<width; i++)
   delete[] image[i];
  delete[] image;
}