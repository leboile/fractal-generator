#include <iostream>
#include <fstream>
#include "fractal.h"
#include "mandelbrotJulia.h"
using namespace std;
int main()
{
int h,w,minx,maxx,miny,maxy,c,p,n,isMbs;
double juliaRe,juliaIm;
char filename[10];
ofstream file;
cout<<"Filename: "<<endl;
cin>>filename; 
 file.open(filename);
cout<<"0-JuliaSet, 1-MandelbrotSet"<<endl;
cin>>isMbs;
cout<<"Input Width and Height of the image:"<<endl;
cin>>w>>h;
cout<<"Input color constant:"<<endl;
cin>>c;
cout<<"Input iterations number:"<<endl;
cin>>n;
if (!isMbs)
{
cout<<"Input x boundaries:"<<endl;
cin>>minx>>maxx;
cout<<"Input y boundaries:"<<endl;
cin>>miny>>maxy;
cout<<"Input julia constant:"<<endl;
cin>>juliaRe>>juliaIm;
cout<<"Input power constant:"<<endl;
cin>>p;
cout<<"Building..."<<endl;
juliaSet mbS(w,h,minx,maxy,miny,maxy,juliaRe,juliaIm,c,p,n);
cout<<"Writing..."<<endl;
 file<<mbS; 
cout<<"DONE"<<endl; 
}
else
{
 cout<<"Building..."<<endl;
 mandelbrotSet mbS(w,h,c,n);
 cout<<"Writing..."<<endl;
  file<<mbS; 
 cout<<"DONE"<<endl;  
}
 file.close();
return 0; 
}