This program produces .pbm textual file to represent graphics, but there is also a version with usage of CImg lib, but it won't compile under modern gcc. There is fractal.png in source file, this image was created by manual conversion of fractal.pbm via online service. fractal.pbm was produced with the following input

$ make run

g++ -ansi -Wall -pedantic -c fractal.cpp -o fractal.o

g++ -ansi -Wall -pedantic -c mandelbrotJulia.cpp fractal.o -o mandelbrotJulia.o

g++: предупреждение: fractal.o: входные файлы компоновки не использованы, поскольку компоновка не выполнялась

g++ -ansi -Wall -pedantic main.cpp fractal.o mandelbrotJulia.o -o res

./res
Filename:

fractal.pbm

0-JuliaSet, 1-MandelbrotSet

1

Input Width and Height of the image:

1024

1024

Input color constant:

64

Input iterations number:

64

Building...

Writing...

DONE