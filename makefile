fractal.o: fractal.cpp
		g++ -ansi -Wall -pedantic -c fractal.cpp -o fractal.o
mandelbrotJulia.o: fractal.o mandelbrotJulia.cpp
		g++ -ansi -Wall -pedantic -c mandelbrotJulia.cpp fractal.o -o mandelbrotJulia.o
res: fractal.o mandelbrotJulia.o main.cpp
		g++ -ansi -Wall -pedantic main.cpp fractal.o mandelbrotJulia.o -o res
run: res
		./res