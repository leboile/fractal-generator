#include "fractal.h"
int* fractal::operator[](int i)
{
 return image[i];
}
void fractal::cSquare(double& Imx, double& Rex)
{ 
 double Imz=Imx*Imx-Rex*Rex;
 double Rez=Rex*Imx+Imx*Rex;
 Imx=Imz;
 Rex=Rez;
}
int fractal::getXY(int x, int y)
{
 return image[x][y];
}
ofstream& operator<<(ofstream& ofs, fractal& fr)
{
 int b[3];
 ofs<<"P3"<<endl<<fr.width<<' '<<fr.height<<endl;
 for (int i=0; i<fr.height; i++)
  {
   for (int j=0; j<fr.width; j++)
   {
    fr.color(j,i,fr.colorConst,b);
    ofs<<b[0]<<' '<<b[1]<<' '<<b[2]<<' ';
    ofs<<endl;
   }   
  }
 return ofs; 
}