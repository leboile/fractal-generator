#include "fractal.h"
#include <math.h>
#ifndef __mandelbrotJulia
#define __mandelbrotJulia
class mandelbrotSet:public fractal
{
public:
mandelbrotSet(int w,int h,int c,int n);
void color(int i, int j,int c,int* rgb);
~mandelbrotSet();
void draw(int n);
};
class juliaSet:public fractal 
{
private:
 int power;
 double juliaConstIm;
 double juliaConstRe;
 int minX;
 int maxX;
 int minY;
 int maxY;
 int maxColors;
public:
juliaSet(int w,int h,int ax, int bx, int ay, int by, double cRe,double cIm,int c,int p, int n);
~juliaSet();
void color(int i, int j,int c,int* rgb);
void draw(int n);
};
#endif