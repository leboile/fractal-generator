#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;
class fractal
{
protected:
int** image;
int height;
int width;
public:
virtual void draw(int)=0;
int* operator[](int i)
{
 return image[i];
}
void cSquare(double& Imx, double& Rex)
{ 
 double Imz=Imx*Imx-Rex*Rex;
 double Rez=Rex*Imx+Imx*Rex;
 Imx=Imz;
 Rex=Rez;
}
int getXY(int x, int y)
{
 return image[x][y];
}
};

class mandelbrotSet:public fractal
{
public:
mandelbrotSet(int w,int h,int n)
{
 height=h;
 width=w;
 image=new int*[w];
 for(int i=0; i<w; i++)
  image[i]=new int[h];
 for(int x=0; x<width; x++)
  for(int y=0; y<height; y++)
    image[x][y]=0;
 draw(n);
}
void draw(int n)
{
  bool belong;
  double Re,prevzRe=0,currzRe=0;
  double Im,prevzIm=0,currzIm=0;
  for(int y=0; y<height; y++)
   for(int x=0; x<width; x++)
   {
    Re=x*(3.5/width)-2; 
    Im=y*(2.0/height)-1;
	//cout<<Re<<" "<<Im<<endl;
	belong=true;
	currzRe=0;
	currzIm=0;
	prevzRe=0;
	prevzIm=0;
	for(int i=0; i<n;i++)
	{
	 cSquare(currzRe,currzIm);
	 currzRe+=Re;
	 currzIm+=Im;
	 if ((currzRe*currzRe+currzIm*currzIm>4) || (currzRe!=currzRe) || (currzIm!=currzIm))
	 {
	  belong=false;
	  break;
	 }
	}
	if (belong)
	 image[x][y]=1; else image[x][y]=0;
  }
}
};
class juliaSet:public fractal 
{
private:
int power;
 double juliaConstIm;
 double juliaConstRe;
 int minX;
 int maxX;
 int minY;
 int maxY;
 int maxColors;
public:
juliaSet(int w,int h,int ax, int bx, int ay, int by, double cRe,double cIm,int p, int c, int n)
{
 height=h;
 width=w;
 power=p;
 minX=ax;
 maxX=bx;
 minY=ay;
 maxY=by;
 maxColors=c;
 juliaConstIm=cIm;
 juliaConstRe=cRe;
 image=new int*[w];
 for(int i=0; i<w; i++)
  image[i]=new int[h];
 for(int x=0; x<width; x++)
  for(int y=0; y<height; y++)
    image[x][y]=0;
 draw(n);
}
void draw(int n)
{
  int k=0;
  bool belong;
  double currzRe=0;
  double currzIm=0;
  for(int y=0; y<height; y++)
   for(int x=0; x<width; x++)
   {
    currzRe=(double)x*((double)(fabs(maxX)+fabs(minX))/(double)width)+minX; 
    currzIm=maxY-((double)y*((double)(fabs(maxY)+fabs(minY))/(double)height));
	belong=true;
	k=0;
	while(k<n)
	{
	 k++;
	 for(int i=0; i<power;i++)
	 cSquare(currzRe,currzIm);
	 currzRe+=juliaConstRe;
	 currzIm+=juliaConstIm;
	// currzRe=prevzRe*prevzRe-prevzIm*prevzIm+juliaConstRe;
	// currzIm=2*prevzIm*prevzRe+juliaConstIm;
	// prevzRe=currzRe;
	// prevzIm=currzIm;
	 //cout<<currzRe*currzRe+currzIm*currzIm<<endl;
	 if ((currzRe*currzRe+currzIm*currzIm>4)||(currzRe!=currzRe) || (currzIm!=currzIm))
	 {
	// cout<<currzRe*currzRe+currzIm*currzIm<<""<<x<<" "<<y<<endl;
	  belong=false;
	  break;
	 }
	}
	if (belong)
	 image[x][y]=1; else image[x][y]=k%maxColors;
  }
}
};
int main()
{
 juliaSet mbS(1024,1024,-2,2,-2,2,0.295,0.1,1,50,10000);
 ofstream file;
 file.open("D:\\f.pbm");
 file<<"P2"<<endl<<"1024"<<' '<<"1024"<<endl<<"15"<<endl;
 for(int i=0; i<1024; i++)
  {
  for(int j=0; j<1024; j++)
   {
	 file<<mbS.getXY(j,i)<<' ';
   }
   file<<endl;
  }
 file.close();
return 0; 
}